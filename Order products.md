# 1. General information

| Attribute | Value |
| --------- | ----- |
| name | Order products |
| key | product.order |
| description | 'Place a new product order' |

# 2. Main results

This Process results in the following:

| When | Result |
| ---- | ------ |
| always | ["products are ordered"](<Order products.md#36-products-are-ordered>) |
| always | ["order has been checked"](<rules.md#11-order-has-been-checked>) |
| always | ["order is approved manually if needed"](<Order products.md#31-order-is-approved-manually-if-needed>) |
| [ORDER](<LDM.md#11-order>).approved | ["invoice is sent"](<messages.md#11-invoice-is-sent>) and ["order report is created"](<messages.md#14-order-report-is-created>) |

# 3. Details

## 3.1. "order is approved manually if needed"

"order is approved manually if needed" results in the following:

| When | Result |
| ---- | ------ |
| ["manual approval reasons have been found"](<Order products.md#32-manual-approval-reasons-have-been-found>) | ["order is approved manually"](<Order products.md#34-order-is-approved-manually>) |
| otherwise | ["order is approved automatically"](<Order products.md#33-order-is-approved-automatically>) |

## 3.2. "manual approval reasons have been found"

exists [ORDER](<LDM.md#11-order>).MANUAL_APPROVAL_REASONS
## 3.3. "order is approved automatically"

"order is approved automatically" results in the following:

The following fields of [ORDER](<LDM.md#11-order>) are set:

| Field | Value |
| ----- | ----- |
| approved | true |

## 3.4. "order is approved manually"

"order is approved manually" results in the following:

The following fields of [ORDER](<LDM.md#11-order>) are set:

| Field | Value |
| ----- | ----- |
| approved | input from EMPLOYEE based on ["info for approval"](<Order products.md#35-info-for-approval>) |

## 3.5. "info for approval"

| Label | Value |
| ----- | ----- |
| none | [ORDER](<LDM.md#11-order>).BUYER |
| none | [ORDER](<LDM.md#11-order>).LINES |
| none | [ORDER](<LDM.md#11-order>).total |
| none | [ORDER](<LDM.md#11-order>).MANUAL_APPROVAL_REASONS |
| Invoice | (displayed_as `html` to ["body of invoice"](<messages.md#12-body-of-invoice>)) |

## 3.6. "products are ordered"

"products are ordered" results in the following:

A [ORDER](<LDM.md#11-order>) is created in [ORDERS](<LDM.md#11-order>) with fields:

| Field | Value |
| ----- | ----- |
| BUYER | CUSTOMER |
| LINES | ["order lines"](<Order products.md#37-order-lines>) |
| comments | input from CUSTOMER |
| delivery_date | input from CUSTOMER labeled 'Preferred delivery date' |

## 3.7. "order lines"

"order lines" results in the following:

Multiple LINE*(s) in [ORDER_LINES](<LDM.md#12-order_line>) are created with:

| Field | Value |
| ----- | ----- |
| nr | (element_index LINE) + 1 |
| PRODUCT | input from CUSTOMER |
| quantity | input from CUSTOMER |
