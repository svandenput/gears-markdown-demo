# 1. Reusable definitions

## 1.1. "order has been checked"

"order has been checked" results in the following:

 - ["if ](<rules.md#15-if-`Too expensive.`-then-add-reason-](<rules.md#15-if-{?}-then-add-reason-{?}-to-{?}>)[ORDER](<LDM.md#11-order>)[-to-{?}>)["too expensive"](<rules.md#12-too-expensive>)[ then add reason {?} to {?}"](<rules.md#15-if-{?}-then-add-reason-{?}-to-{?}>)
 - ["if ](<rules.md#15-if-`Another reason.`-then-add-reason-](<rules.md#15-if-{?}-then-add-reason-{?}-to-{?}>)[ORDER](<LDM.md#11-order>)[-to-{?}>)["another reason why manual approval is needed"](<rules.md#13-another-reason-why-manual-approval-is-needed>)[ then add reason {?} to {?}"](<rules.md#15-if-{?}-then-add-reason-{?}-to-{?}>)
 - ["if ](<rules.md#15-if-`Yet another reason.`-then-add-reason-](<rules.md#15-if-{?}-then-add-reason-{?}-to-{?}>)[ORDER](<LDM.md#11-order>)[-to-{?}>)["yet another reason why manual approval is needed"](<rules.md#14-yet-another-reason-why-manual-approval-is-needed>)[ then add reason {?} to {?}"](<rules.md#15-if-{?}-then-add-reason-{?}-to-{?}>)

## 1.2. "too expensive"

 - [ORDER](<LDM.md#11-order>).total \> 500.0

## 1.3. "another reason why manual approval is needed"

 - false

## 1.4. "yet another reason why manual approval is needed"

 - false

## 1.5. "if c then add reason c to c"

"if c then add reason c to c" results in the following:

| When | Result |
| ---- | ------ |
| c | A R is created in [REASONS](<LDM.md#14-reason>) with fields:<table><thead><tr><th>Field</th><th>Value</th></tr></thead><tbody><tr><td>text</td><td>r</td></tr><tr><td>RELATED_ORDER</td><td>THIS_ORDER</td></tr></tbody></table> |
