# 1. Reusable definitions

## 1.1. "� p"

`��`p
## 1.2. "styled html with content"

`<!DOCTYPE�html>`  <br>`����<html>`  <br>`����<head>`  <br>`��������<style>`  <br>`������������body�{`  <br>`����������������font-family:�Sans-serif;`  <br>`������������}`  <br>` `  <br>`������������table�{`  <br>`����������������border-collapse:�collapse;`  <br>`����������������margin:�25px�0;`  <br>`����������������font-size:�0.9em;`  <br>`����������������font-family:�sans-serif;`  <br>`����������������min-width:�400px;`  <br>`����������������box-shadow:�0�0�20px�rgba(0,�0,�0,�0.15);`  <br>`������������}`  <br>` `  <br>`������������thead�tr�{`  <br>`����������������font-weight:�bold;`  <br>`����������������background-color:�#009879;`  <br>`����������������color:�#ffffff;`  <br>`������������}`  <br>` `  <br>`������������tfoot�tr�{`  <br>`����������������font-weight:�bold;`  <br>`����������������border-top:�2px�solid�gray;`  <br>`������������}`  <br>` `  <br>`������������th,`  <br>`������������td�{`  <br>`����������������padding:�12px�15px;`  <br>`������������}`  <br>` `  <br>`������������tbody�tr�{`  <br>`����������������border-bottom:�1px�solid�#dddddd;`  <br>` `  <br>`������������}`  <br>` `  <br>`������������tbody�tr:nth-of-type(even)�{`  <br>`����������������background-color:�#f3f3f3;`  <br>`������������}`  <br>` `  <br>`������������tbody�tr.active�{`  <br>`����������������font-weight:�bold;`  <br>`����������������color:�#009879;`  <br>`������������}`  <br>` `  <br>`������������tbody�tr:hover�{`  <br>`����������������background-color:�#baebd5;`  <br>`����������������box-shadow:�0�0�20px�rgba(0,�0,�0,�0.15);`  <br>`����������������transition:�0.5s;`  <br>`������������}`  <br>`��������</style>`  <br>`����</head>`  <br>`����<body>`content`</body>`  <br>`����</html>`
## 1.3. "table with table_content"

`<table>`  <br>`��������`table_content` `  <br>`����</table>`
## 1.4. "table header with TITLES"

`<thead>�<tr>��`(concat ["table cells with TITLES"](<standard reusable definitions.md#19-table-cells-with-{?}>))`�</tr>�</thead>`  <br>`����`
## 1.5. "table body with table_body_content"

`<tbody>`  <br>`��������`table_body_content` `  <br>`����</tbody>`
## 1.6. "table row with DATA"

`<tr>��`(concat ["table cells with DATA"](<standard reusable definitions.md#19-table-cells-with-{?}>))`�</tr>`  <br>`����`
## 1.7. "table row with DATA styled as DATA"

`<tr�class="`styling_class`">��`(concat ["table cells with DATA"](<standard reusable definitions.md#19-table-cells-with-{?}>))`�</tr>`  <br>`����`
## 1.8. "table footer with DATA"

`<tfoot>�<tr>��`(concat ["table cells with DATA"](<standard reusable definitions.md#19-table-cells-with-{?}>))`�</tr>�</tfoot>`
## 1.9. "table cells with DATA"

For each D in the DATA:

<blockquote>`<td>�`D`�</td>`</blockquote>
