# 1. Reusable definitions

## 1.1. "invoice is sent"

"invoice is sent" results in the following:

A INVOICE is created in MAILS with fields:

| Field | Value |
| ----- | ----- |
| from_address | `noreply@littlespider.com` |
| to_address | [ORDER](<LDM.md#11-order>).BUYER.email_address |
| subject | `Your�invoice�nr�`[ORDER](<LDM.md#11-order>).nr`�from�`[ORDER](<LDM.md#11-order>).date_ordered |
| body | ["body of invoice"](<messages.md#12-body-of-invoice>) |
| content_type | `text/html` |
| attachments | (to_files [ORDER_REPORT, DOCX_TO_DOCX_ORDER_REPORT, DOCX_TO_PDF_ORDER_REPORT]) |

## 1.2. "body of invoice"

 - ["styled html with ` `  <br>`Dear�mr/ms�`](<standard reusable definitions.md#12-styled-html-with-{?}>)[ORDER](<LDM.md#11-order>).BUYER.family_name`,�<br/>`  <br>`<br/>`  <br>`Thank�you�for�placing�an�order�with�us�today.�`  <br>`We�will�send�the�products�as�soon�as�possible.�`  <br>`Here�is�a�list�of�the�products�you�have�ordered:`["table with ](<standard reusable definitions.md#13-table-with-{?}>)["table header with [`PRODUCT`, `PRICE�(�)`, `Ordered�products`, `TOTAL�(�)`]"](<standard reusable definitions.md#14-table-header-with-{?}>)[ + ](<standard reusable definitions.md#13-table-with-{?}>)["table body with (concat ](<standard reusable definitions.md#15-table-body-with-{?}>)["table rows"](<messages.md#13-table-rows>)[)"](<standard reusable definitions.md#15-table-body-with-{?}>)[ + ](<standard reusable definitions.md#13-table-with-{?}>)["table footer with [`TOTAL`, , , [ORDER](<LDM.md#11-order>).total]"](<standard reusable definitions.md#18-table-footer-with-{?}>)["](<standard reusable definitions.md#13-table-with-{?}>)` `  <br>`You�had�the�following�comments�about�this�order:<br/>`(escaped `html` to [ORDER](<LDM.md#11-order>).comments)`<br/>`  <br>`<br/>`  <br>`Kind�regards,�<br/>`  <br>`<br/>`  <br>`Your�company�system`"](<standard reusable definitions.md#12-styled-html-with-{?}>)

## 1.3. "table rows"

For each L in the [ORDER](<LDM.md#11-order>).LINES:

<blockquote>["table row with [L.PRODUCT.name, L.PRODUCT.price, L.quantity, L.total]"](<standard reusable definitions.md#16-table-row-with-{?}>)</blockquote>

## 1.4. "order report is created"

"order report is created" results in the following:

A ORDER_REPORT is created in DOCUMENTS with fields:

 - A ORDER_REPORT is created in DOCUMENTS with fields:

| Field | Value |
| ----- | ----- |
| type | `pdf` |
| title | `Order�`[ORDER](<LDM.md#11-order>).nr`�for�`[ORDER](<LDM.md#11-order>).BUYER.full_name`�(jasper�reports)` |
| filename | `Order�`[ORDER](<LDM.md#11-order>).nr`�for�`[ORDER](<LDM.md#11-order>).BUYER.full_name`�(jasper�reports).pdf` |
| template | `/templates/order.jrxml` |
| parameters | ["order report parameters"](<messages.md#15-order-report-parameters>) |

 - A DOCX_TO_DOCX_ORDER_REPORT is created in DOCUMENTS with fields:

| Field | Value |
| ----- | ----- |
| type | `docx` |
| title | `Order�`[ORDER](<LDM.md#11-order>).nr`�for�`[ORDER](<LDM.md#11-order>).BUYER.full_name`�(xdoc�reports)` |
| filename | `Order�`[ORDER](<LDM.md#11-order>).nr`�for�`[ORDER](<LDM.md#11-order>).BUYER.full_name`�(xdoc�reports).docx` |
| template | `/templates/order-template.docx` |
| parameters | ["order report parameters"](<messages.md#15-order-report-parameters>) |

 - A DOCX_TO_PDF_ORDER_REPORT is created in DOCUMENTS with fields:

| Field | Value |
| ----- | ----- |
| type | `pdf` |
| title | `Order�`[ORDER](<LDM.md#11-order>).nr`�for�`[ORDER](<LDM.md#11-order>).BUYER.full_name`�(xdoc�reports)` |
| filename | `Order�`[ORDER](<LDM.md#11-order>).nr`�for�`[ORDER](<LDM.md#11-order>).BUYER.full_name`�(xdoc�reports).pdf` |
| template | `/templates/order-template.docx` |
| parameters | ["order report parameters"](<messages.md#15-order-report-parameters>) |


## 1.5. "order report parameters"

| Label | Value |
| ----- | ----- |
| buyer_name | [ORDER](<LDM.md#11-order>).BUYER.full_name |
| date_ordered | [ORDER](<LDM.md#11-order>).date_ordered |
| comments | [ORDER](<LDM.md#11-order>).comments |
| total | ["format amount ](<messages.md#17-format-amount-{?}>)[ORDER](<LDM.md#11-order>)[.total"](<messages.md#17-format-amount-{?}>) |
| lines | ["order table"](<messages.md#16-order-table>) |

## 1.6. "order table"

For each L in the [ORDER](<LDM.md#11-order>).LINES:

<blockquote>{ name : L.PRODUCT.name, price : ["format amount L.PRODUCT.price"](<messages.md#17-format-amount-{?}>), quantity : L.quantity, subtotal : ["format amount L.total"](<messages.md#17-format-amount-{?}>) }</blockquote>

## 1.7. "format amount value"

(to_text value to `��#,###` to `NL`)