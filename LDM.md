# 1. Reusable definitions
## 1.1. ORDER
A ORDER in ORDERS has fields:

| Field | Value |
| ----- | ----- |
| nr | number autovalue sequence("seq_order_nr") |
| date_ordered | date autovalue current_date() |
| BUYER | USER |
| LINES | multiple [ORDER_LINE](<LDM.md#12-order_line>) opposite of ORDER |
| MANUAL_APPROVAL_REASONS | multiple [REASON](<LDM.md#14-reason>) optional true, opposite of RELATED_ORDER |
| approved | boolean |
| comments | text |
| delivery_date | date |
| total | number(10,2) = sum(LINES.total) |
## 1.2. ORDER_LINE
A ORDER_LINE in ORDER_LINES has fields:

| Field | Value |
| ----- | ----- |
| nr | number |
| PRODUCT | [PRODUCT](<LDM.md#13-product>) |
| quantity | number |
| ORDER | [ORDER](<LDM.md#11-order>) opposite of LINES |
| total | number(10,2) = quantity * PRODUCT.price |
| summary | text displayed true, = "{quantity}x {PRODUCT.name} @ {PRODUCT.price}" |
## 1.3. PRODUCT
A PRODUCT in PRODUCTS has fields:

| Field | Value |
| ----- | ----- |
| name | text |
| price | number(10,2) |
| remark | text |
## 1.4. REASON
A REASON in REASONS has fields:

| Field | Value |
| ----- | ----- |
| text | text displayed true |
| RELATED_ORDER | [ORDER](<LDM.md#11-order>) opposite of MANUAL_APPROVAL_REASONS |
