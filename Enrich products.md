# 1. General information

| Attribute | Value |
| --------- | ----- |
| name | Enrich products |
| key | product.enrich |
| starter | EMPLOYEE |
| description | 'Set product remarks' |

# 2. Main results

This Process results in the following:

 - ["first price has been raised"](<Enrich products.md#31-first-price-has-been-raised>)
 - ["prices have been raised"](<Enrich products.md#32-prices-have-been-raised>)
 - ["remarks have been added"](<Enrich products.md#33-remarks-have-been-added>)

# 3. Details

## 3.1. "first price has been raised"

"first price has been raised" results in the following:

For the first FIRST_PRODUCT in [PRODUCTS](<LDM.md#13-product>) sorted:

<blockquote>price = current FIRST_PRODUCT.price * 1.5</blockquote>

## 3.2. "prices have been raised"

"prices have been raised" results in the following:

For each [PRODUCT](<LDM.md#13-product>) in the [PRODUCTS](<LDM.md#13-product>):

<blockquote>price = current [PRODUCT](<LDM.md#13-product>).price * 1.2</blockquote>

## 3.3. "remarks have been added"

"remarks have been added" results in the following:

For each [PRODUCT](<LDM.md#13-product>) in the [PRODUCTS](<LDM.md#13-product>):

<blockquote>remark = input from EMPLOYEE based on { [PRODUCT](<LDM.md#13-product>).name, [PRODUCT](<LDM.md#13-product>).price }</blockquote>
