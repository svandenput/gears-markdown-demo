# 1. General information

| Attribute | Value |
|-----------|-----------|
| name | Request absence |
| subject | ["info for decision"](<Request absence.md#35-info-for-decision>) |
| key | loa.absence.request |
| description | 'Request a leave of absence' |

# 2. Main results

This Process results in the following:

| When | Result |
| ---- | ------ |
| always | ["a leave of absence is requested"](<Request absence.md#32-a-leave-of-absence-is-requested>) |
| always | ["a manual decision is made if needed"](<Request absence.md#31-a-manual-decision-is-made-if-needed>) |
| always | ["decision mail is sent to employee"](<Request absence.md#36-decision-mail-is-sent-to-employee>) |

# 3. Details

## 3.1. "a manual decision is made if needed"

"a manual decision is made if needed" results in the following:

| When | Result |
| ---- | ------ |
| ["manual decision needed"](<rules.md#11-manual-decision-needed>) | ["a manual decision is made"](<Request absence.md#33-a-manual-decision-is-made>) |
| otherwise | ["an automatic decision is made"](<Request absence.md#34-an-automatic-decision-is-made>) |

## 3.2. "a leave of absence is requested"

"a leave of absence is requested" results in the following:

A LEAVE_REQUEST is created in [LEAVE_OF_ABSENCE_REQUESTS](<LDM.md#11-LEAVE_OF_ABSENCE_REQUEST>) with fields:

| Field | Value |
| ----- | ----- |
| REQUESTOR | EMPLOYEE |
| type | input from EMPLOYEE (with default value of 'holiday') |
| start | input from EMPLOYEE |
| end | input from EMPLOYEE |

## 3.3. "a manual decision is made"

"a manual decision is made" results in the following:

The following fields of LEAVE_REQUEST are edited

| Field | Value |
| ----- | ----- |
| decision | input from MANAGER based on ["info for decision"](<Request absence.md#35-info-for-decision>) (with default value of 'approved') |
| reason | input from MANAGER based on ["info for decision"](<Request absence.md#35-info-for-decision>) |

## 3.4. "an automatic decision is made"

"an automatic decision is made" results in the following:

The following fields of LEAVE_REQUEST are edited

| Field | Value |
| ----- | ----- |
| decision | 'approved' |
| reason | 'your request has been processed straight through' |

## 3.5. "info for decision"

"info for decision" results in the following:

| Key | Value |
| --- | ----- |
| Requestor | LEAVE_REQUEST.REQUESTOR.given_name + ' ' + LEAVE_REQUEST.REQUESTOR.family_name |
| null | LEAVE_REQUEST.type |
| null | LEAVE_REQUEST.start |
| null | LEAVE_REQUEST.end |

## 3.6. "decision mail is sent to employee"

"decision mail is sent to employee" results in the following:

A MAIL is created in MAILS with fields:

| Field | Value |
| ----- | ----- |
| to_address | EMPLOYEE.email_address |
| from_address | 'hr@acme.xlrit.com' |
| subject | 'Your leave of absence request' |
| body | ' Dear EMPLOYEE.given_name, </br> </br> Your LEAVE_REQUEST.type </br> from LEAVE_REQUEST.start - LEAVE_REQUEST.end </br> is LEAVE_REQUEST.decision </br> with reason: LEAVE_REQUEST.reason </br> </br> Kinds regards, </br> Your GEARS powered system. ' |
