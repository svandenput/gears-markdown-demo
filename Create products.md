# 1. General information

| Attribute | Value |
| --------- | ----- |
| name | Create products |
| key | product.create |
| description | 'Create a desired number of products' |

# 2. Main results

This Process results in the following:

 - ["products are created"](<Create products.md#31-products-are-created>)

# 3. Details

## 3.1. "products are created"

"products are created" results in the following:

For the only count in [input from MANAGER labeled 'Number of products to create' of type number]:

<blockquote>For each nr in the (range 1 to count):

<blockquote>["product nr is created"](<Create products.md#32-product-{?}-is-created>)</blockquote>
</blockquote>

## 3.2. "product nr is created"

"product nr is created" results in the following:

A [PRODUCT](<LDM.md#13-product>) is created in [PRODUCTS](<LDM.md#13-product>) with fields:

| Field | Value |
| ----- | ----- |
| name | `Product `nr |
| price | (random_number 1 to 100) * 1.23 |
| remark | `Remark for `nr |
